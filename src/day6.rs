use crate::OrError;
use clap::Parser;
use color_eyre::eyre::Context;
use eyre::eyre;
use std::collections::HashSet;

#[derive(Parser)]
pub struct Opts {
    file: String,
}

pub fn run(opts: Opts) -> OrError<()> {
    let signal = std::fs::read_to_string(&opts.file).wrap_err(format!("Reading {}", opts.file))?;
    let part1 = solve::<4>(&signal)?;
    let part2 = solve::<14>(&signal)?;
    println!("Part1: {part1}",);
    println!("Part2: {part2}",);
    Ok(())
}

fn solve<const N: usize>(str: &str) -> OrError<usize> {
    let mut ring: Ring<N> = Ring::new();
    let mut res = None;
    for (idx, ch) in str.chars().enumerate() {
        ring.push(ch);
        if ring.has_unique_elem() && res.is_none() {
            res = Some(idx + 1);
        }
    }
    res.ok_or_else(|| eyre!("pattern not found"))
}

struct Ring<const N: usize> {
    pos: usize,
    added: usize,
    buf: [char; N],
}

impl<const N: usize> Ring<N> {
    fn new() -> Self {
        Ring {
            added: 0,
            pos: 0,
            buf: ['#'; N],
        }
    }

    fn push(&mut self, ch: char) {
        self.added += 1;
        self.buf[self.pos] = ch;
        self.pos = (self.pos + 1) % N;
    }

    fn has_unique_elem(&self) -> bool {
        self.added >= N && self.buf.iter().cloned().collect::<HashSet<char>>().len() == N
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use test_case::test_case;

    #[test_case("mjqjpqmgbljsphdztnvjfqwrcgsmlb", 7)]
    #[test_case("nppdvjthqldpwncqszvftbrmjlhg", 6)]
    #[test_case("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg", 10)]
    #[test_case("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw", 11)]
    fn part1(str: &str, res: usize) -> OrError<()> {
        assert_eq!(solve::<4>(str)?, res);
        Ok(())
    }

    #[test_case("mjqjpqmgbljsphdztnvjfqwrcgsmlb", 19)]
    #[test_case("bvwbjplbgvbhsrlpgdmjqwftvncz", 23)]
    #[test_case("nppdvjthqldpwncqszvftbrmjlhg", 23)]
    #[test_case("nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg", 29)]
    #[test_case("zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw", 26)]
    fn part2(str: &str, res: usize) -> OrError<()> {
        assert_eq!(solve::<14>(str)?, res);
        Ok(())
    }
}
