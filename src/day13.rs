use crate::OrError;
use clap::Parser;
use color_eyre::eyre::Context;
use eyre::eyre;
use itertools::{EitherOrBoth, Itertools};
use log::{debug, log_enabled, Level::Debug};
use std::{
    fmt,
    fs::File,
    io::{BufRead, BufReader},
};

#[derive(Parser)]
pub struct Opts {
    file: String,
}

pub fn run(opts: Opts) -> OrError<()> {
    let file = BufReader::new(File::open(&opts.file).wrap_err(format!("Reading {}", opts.file))?);
    itertools::process_results(file.lines(), |lines| {
        let groups = lines.into_iter().group_by(|str| str.is_empty());
        let pairs: Vec<(Value, Value)> = groups
            .into_iter()
            .filter_map(|(empty, l)| {
                if empty {
                    None
                } else {
                    l.collect::<Vec<_>>().into_iter().collect_tuple()
                }
            })
            .map(|(s1, s2)| Ok::<_, eyre::Error>((parse_value(&s1)?, parse_value(&s2)?)))
            .try_collect()?;
        let mut part1 = 0;
        for (idx, (p1, p2)) in pairs.iter().enumerate() {
            debug!("Pair 1: {p1}");
            debug!("Pair 2: {p2}");
            let order = p1.cmp(p2);
            debug!("Order: {:?}", order);
            debug!("");
            if matches!(order, std::cmp::Ordering::Less) {
                part1 += 1 + idx;
            }
        }
        let pkt1 = Value::List(vec![Value::List(vec![Value::Integer(2)])]);
        let pkt2 = Value::List(vec![Value::List(vec![Value::Integer(6)])]);
        let sorted = std::iter::once(pkt1.clone())
            .chain(std::iter::once(pkt2.clone()))
            .chain(pairs.into_iter().flat_map(|(p1, p2)| vec![p1, p2]))
            .sorted()
            .collect::<Vec<_>>();
        if log_enabled!(Debug) {
            debug!("Sorted:");
            for v in &sorted {
                debug!("{v}");
            }
        }
        println!("Part 1: {part1}");
        let idx1 = sorted.iter().position(|x| x == &pkt1).unwrap() + 1;
        let idx2 = sorted.iter().position(|x| x == &pkt2).unwrap() + 1;
        println!("Part 2: {}", idx1 * idx2);
        Ok::<(), eyre::Error>(())
    })??;
    Ok(())
}

#[derive(Clone, Debug)]
enum Value {
    Integer(u64),
    List(Vec<Value>),
}

impl fmt::Display for Value {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Value::Integer(n) => write!(f, "{n}"),
            Value::List(vs) => write!(f, "[{}]", vs.iter().map(|v| v.to_string()).join(", ")),
        }
    }
}

impl PartialEq for Value {
    fn eq(&self, other: &Self) -> bool {
        matches!(self.cmp(other), std::cmp::Ordering::Equal)
    }
}

impl Eq for Value {}

impl PartialOrd for Value {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Value {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        use std::cmp::Ordering::*;
        use Value::*;
        match (self, other) {
            (Integer(n1), Integer(n2)) => n1.cmp(n2),
            (List(l1), List(l2)) => l1
                .iter()
                .zip_longest(l2.iter())
                .into_iter()
                .find_map(|x| match x {
                    EitherOrBoth::Left(_) => Some(Greater),
                    EitherOrBoth::Right(_) => Some(Less),
                    EitherOrBoth::Both(v1, v2) => match v1.cmp(v2) {
                        Equal => None,
                        Greater => Some(Greater),
                        Less => Some(Less),
                    },
                })
                .unwrap_or(Equal),
            (Integer(n1), List(l2)) => List(vec![Integer(*n1)]).cmp(&List(l2.to_vec())),
            (List(l1), Integer(n2)) => List(l1.to_vec()).cmp(&List(vec![Integer(*n2)])),
        }
    }
}

fn parse_value(str: &str) -> eyre::Result<Value> {
    use nom::{
        branch::alt,
        bytes::complete::tag,
        character::complete::{space0, u64},
        combinator::{all_consuming, map},
        multi::separated_list0,
        sequence::{delimited, terminated},
        IResult,
    };
    fn value(str: &str) -> IResult<&str, Value> {
        alt((
            map(u64, Value::Integer),
            map(
                delimited(
                    tag("["),
                    separated_list0(terminated(tag(","), space0), value),
                    tag("]"),
                ),
                Value::List,
            ),
        ))(str)
    }
    let (_, value) =
        all_consuming(value)(str).map_err(|err| eyre!("Error parsing value '{str}': {err}"))?;
    Ok(value)
}
