use crate::OrError;
use clap::Parser;
use color_eyre::eyre::Context;
use eyre::{bail, eyre};
use log::debug;
use std::{
    fmt,
    fs::File,
    io::{BufRead, BufReader},
};

#[derive(Parser)]
pub struct Opts {
    file: String,

    #[clap(long)]
    has_floor: bool,
}

const SOURCE_X: usize = 500;
const SOURCE_Y: usize = 0;
const SAND_FALL: &[(isize, isize)] = &[(0, 1), (-1, 1), (1, 1)];

pub fn run(opts: Opts) -> OrError<()> {
    let file = BufReader::new(File::open(&opts.file).wrap_err(format!("Reading {}", opts.file))?);
    itertools::process_results(file.lines(), |lines| {
        let mut cave = Cave::new();
        for l in lines {
            let path = parse_path(&l)?;
            debug!("Path: {path}");
            cave.add_path(path)?;
        }
        if opts.has_floor {
            cave.add_path(Path {
                points: vec![
                    (
                        cave.min_x().saturating_sub(cave.height + 1),
                        cave.height + 1,
                    ),
                    (cave.max_x() + cave.height + 1, cave.height + 1),
                ],
            })?;
        }
        debug!("Cave:\n{cave}");
        let mut sand_count = 0;
        while cave.drop_sand() {
            sand_count += 1;
            debug!("Cave:\n{cave}");
        }
        debug!("Cave:\n{cave}");
        println!("Part 1: {sand_count}");
        Ok::<(), eyre::Error>(())
    })??;
    Ok(())
}

#[derive(Debug)]
struct Cave {
    grid: Vec<Vec<Cell>>,
    height: usize,
    width: usize,
}

impl Cave {
    fn new() -> Self {
        let mut grid = vec![vec![Cell::Air; SOURCE_X + 1]];
        grid[SOURCE_Y][SOURCE_X] = Cell::Source;
        Self {
            grid,
            height: SOURCE_Y + 1,
            width: SOURCE_X + 1,
        }
    }

    fn add_path(&mut self, path: Path) -> eyre::Result<()> {
        use std::cmp::{max, min};
        let mut points = path.points.into_iter();
        let Some((mut sx, mut sy)) = points.next() else {
            bail!("Empty path");
        };
        self.resize_for(sx, sy);
        for (x, y) in points {
            debug!("Point {x},{y}");
            self.resize_for(x, y);
            if x == sx {
                let y0 = min(sy, y);
                let y1 = max(sy, y);
                for i in y0..=y1 {
                    debug!("Filling {i},{x}");
                    self.grid[i][x] = Cell::Rock;
                }
            } else {
                let x0 = min(sx, x);
                let x1 = max(sx, x);
                for j in x0..=x1 {
                    debug!("Filling {y},{j}");
                    self.grid[y][j] = Cell::Rock;
                }
            }
            sx = x;
            sy = y;
        }
        Ok(())
    }

    fn resize_for(&mut self, x: usize, y: usize) {
        if x >= self.width {
            self.width = x + 1;
            for l in &mut self.grid {
                l.resize(self.width, Cell::Air);
            }
        }
        if y >= self.height {
            self.height = y + 1;
            self.grid.resize(self.height, vec![Cell::Air; self.width]);
        }
    }

    fn min_x(&self) -> usize {
        self.grid
            .iter()
            .map(|l| {
                l.iter()
                    .position(|c| !matches!(c, Cell::Air))
                    .unwrap_or(usize::MAX)
            })
            .min()
            .unwrap_or(0)
    }

    fn max_x(&self) -> usize {
        self.width - 1
    }

    fn drop_sand(&mut self) -> bool {
        let mut sx = SOURCE_X;
        let mut sy = SOURCE_Y;
        if matches!(self.grid[sy][sx], Cell::Sand) {
            debug!("Sand could not fall from source");
            return false;
        }
        'outer: loop {
            for (dx, dy) in SAND_FALL {
                let x = sx as isize + dx;
                let y = sy as isize + dy;
                if 0 <= x
                    && x < self.width as isize
                    && 0 <= y
                    && y < self.height as isize
                    && matches!(self.grid[y as usize][x as usize], Cell::Air)
                {
                    sx = x as usize;
                    sy = y as usize;
                    continue 'outer;
                }
            }
            if sy == self.height - 1 {
                debug!("Sand fell into the abyss at {sx}");
                return false;
            } else {
                self.grid[sy][sx] = Cell::Sand;
                return true;
            }
        }
    }
}

impl fmt::Display for Cave {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let min_x = self.min_x();
        for l in &self.grid {
            for c in l.iter().skip(min_x.saturating_sub(1)) {
                write!(f, "{c}")?;
            }
            writeln!(f)?;
        }
        Ok(())
    }
}

#[derive(Clone, Copy, Debug, Default)]
enum Cell {
    Source,
    #[default]
    Air,
    Rock,
    Sand,
}

impl fmt::Display for Cell {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Cell::Source => write!(f, "+"),
            Cell::Air => write!(f, " "),
            Cell::Rock => write!(f, "#"),
            Cell::Sand => write!(f, "o"),
        }
    }
}

#[derive(Debug)]
struct Path {
    points: Vec<(usize, usize)>,
}

impl fmt::Display for Path {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut iter = self.points.iter();
        if let Some((x, y)) = iter.next() {
            write!(f, "({x}, {y})")?;
        }
        for (x, y) in iter {
            write!(f, " -> ({x}, {y})")?;
        }
        Ok(())
    }
}

fn parse_path(str: &str) -> eyre::Result<Path> {
    use nom::{
        bytes::complete::tag,
        character::complete::{space0, u64},
        combinator::{all_consuming, map},
        multi::separated_list1,
        sequence::{preceded, separated_pair, terminated},
        IResult,
    };
    fn point(str: &str) -> IResult<&str, (usize, usize)> {
        separated_pair(map(u64, |x| x as usize), tag(","), map(u64, |y| y as usize))(str)
    }
    fn path(str: &str) -> IResult<&str, Path> {
        map(
            separated_list1(preceded(space0, terminated(tag("->"), space0)), point),
            |points| Path { points },
        )(str)
    }
    let (_, value) =
        all_consuming(path)(str).map_err(|err| eyre!("Error parsing path '{str}': {err}"))?;
    Ok(value)
}
