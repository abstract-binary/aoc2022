Advent of Code 2022 - Solutions in Rust
=======================================

Build & Run
-----------

```
$ nix develop
$ cargo build
$ cargo run -- --help
```

License
-------

All code under GPLv3.
