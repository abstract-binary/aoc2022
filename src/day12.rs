use crate::OrError;
use clap::Parser;
use color_eyre::eyre::Context;
use log::debug;
use std::{
    collections::VecDeque,
    fs::File,
    io::{BufRead, BufReader},
};

#[derive(Parser)]
pub struct Opts {
    file: String,
}

const END_POS_H: u64 = u64::MAX - 1;

const ADJACENT: &[(i32, i32)] = &[(0, 1), (0, -1), (1, 0), (-1, 0)];

pub fn run(opts: Opts) -> OrError<()> {
    let file = BufReader::new(File::open(&opts.file).wrap_err(format!("Reading {}", opts.file))?);
    itertools::process_results(file.lines(), |lines| {
        let lines = lines
            .map(|s| s.as_bytes().to_vec())
            .collect::<Vec<Vec<u8>>>();
        let mut dist = lines
            .iter()
            .map(|l| vec![u64::MAX; l.len()])
            .collect::<Vec<Vec<_>>>();
        let mut bfs_queue = VecDeque::new();
        let end_pos = lines
            .iter()
            .enumerate()
            .find_map(|(i, l)| l.iter().position(|c| *c == b'E').map(|j| (i, j)))
            .expect("could not find start pos");
        dist[end_pos.0][end_pos.1] = END_POS_H;
        bfs_queue.push_back(end_pos);
        debug!("End pos: {:?}", end_pos);
        let mut dist_to_start = u64::MAX;
        let mut min_dist = 0;
        while let Some((i, j)) = bfs_queue.pop_front() {
            let mut neighbours = vec![];
            let here = height(lines[i][j]);
            for (di, dj) in ADJACENT {
                let i = i as i32;
                let j = j as i32;
                if 0 <= i + di
                    && i + di < lines.len() as i32
                    && 0 <= j + dj
                    && j + dj < lines[0].len() as i32
                    && here - height(lines[(i + di) as usize][(j + dj) as usize]) <= 1
                {
                    neighbours.push(((i + di) as usize, (j + dj) as usize));
                }
            }
            for (ni, nj) in neighbours {
                if dist[ni][nj] == u64::MAX {
                    dist[ni][nj] = dist[i][j] - 1;
                    debug!("Height a ({ni}, {nj}) = {}", END_POS_H - dist[ni][nj]);
                    if lines[ni][nj] == b'S' {
                        dist_to_start = dist[ni][nj];
                    }
                    if lines[ni][nj] == b'a' {
                        // The `min_dist` corresponds to the max
                        // height of an 'a' tile.
                        min_dist = std::cmp::max(min_dist, dist[ni][nj]);
                    }
                    bfs_queue.push_back((ni, nj));
                }
            }
        }
        println!("Part 1: {}", END_POS_H - dist_to_start);
        println!("Part 2: {}", END_POS_H - min_dist);
        Ok::<(), eyre::Error>(())
    })??;
    Ok(())
}

fn height(h: u8) -> i32 {
    if h == b'S' {
        b'a' as i32
    } else if h == b'E' {
        b'z' as i32
    } else {
        h as i32
    }
}
