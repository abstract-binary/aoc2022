use crate::OrError;
use clap::Parser;
use color_eyre::eyre::Context;
use eyre::bail;
use log::debug;
use std::{
    fmt,
    fs::File,
    io::{BufRead, BufReader},
};

#[derive(Parser)]
pub struct Opts {
    file: String,

    #[clap(long)]
    signal_strength: bool,
}

pub fn run(opts: Opts) -> OrError<()> {
    let file = BufReader::new(File::open(&opts.file).wrap_err(format!("Reading {}", opts.file))?);
    itertools::process_results(file.lines(), |lines| {
        itertools::process_results(lines.map(|str| parse_op(&str)), |ops| {
            let vm = Vm::new(ops);
            if opts.signal_strength {
                let part1: i64 = vm.signal_strengths().take(6).sum();
                println!("Part 1: {part1:?}");
            } else {
                println!("Part 2");
                println!("{}", Crt::from_vm(vm));
            }
        })
    })??;
    Ok(())
}

struct Crt {
    screen: [[bool; 40]; 6],
}

impl Crt {
    fn from_vm(vm: impl Iterator<Item = (u64, i64)>) -> Self {
        let mut screen = [[false; 40]; 6];
        for (cycle, reg) in vm {
            let i = (((cycle - 1) / 40) % 6) as usize;
            let j = ((cycle - 1) % 40) as usize;
            debug!("VM[{cycle}] = {reg}; screen at {i} x {j}");
            if (reg - j as i64).abs() <= 1 {
                screen[i][j] = true;
            }
        }
        Self { screen }
    }
}

impl fmt::Display for Crt {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for i in 0..6 {
            for j in 0..40 {
                write!(f, "{}", if self.screen[i][j] { '#' } else { ' ' })?;
            }
            writeln!(f)?;
        }
        Ok(())
    }
}

#[derive(Debug)]
pub(crate) struct Vm<I> {
    reg: i64,
    cycle: u64,
    pending_addx: Option<i64>,
    first_next: bool,
    input: I,
}

impl<I> Vm<I> {
    fn new(input: I) -> Self {
        Vm {
            reg: 1,
            cycle: 1,
            pending_addx: None,
            first_next: true,
            input,
        }
    }
}

impl<I: Iterator<Item = Op>> Vm<I> {
    fn signal_strengths(self) -> impl Iterator<Item = i64> {
        self.filter_map(|(cycle, reg)| {
            if (cycle as i64 - 20) % 40 == 0 {
                Some(cycle as i64 * reg)
            } else {
                None
            }
        })
    }
}

impl<I: Iterator<Item = Op>> Iterator for Vm<I> {
    type Item = (u64, i64);

    fn next(&mut self) -> Option<Self::Item> {
        if self.first_next {
            self.first_next = false;
            Some((self.cycle, self.reg))
        } else {
            match self.pending_addx {
                Some(x) => {
                    self.cycle += 1;
                    self.reg += x;
                    self.pending_addx = None;
                    Some((self.cycle, self.reg))
                }
                None => match self.input.next() {
                    None => None,
                    Some(Op::Noop) => {
                        self.cycle += 1;
                        Some((self.cycle, self.reg))
                    }
                    Some(Op::Addx(x)) => {
                        self.cycle += 1;
                        self.pending_addx = Some(x);
                        Some((self.cycle, self.reg))
                    }
                },
            }
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub(crate) enum Op {
    Noop,
    Addx(i64),
}

fn parse_op(str: &str) -> eyre::Result<Op> {
    match parser::op(str) {
        Err(err) => bail!("Error parsing '{str}': {err}"),
        Ok(("", motion)) => Ok(motion),
        Ok((left_over, _)) => bail!("Left-over parsing '{str}': '{left_over}'"),
    }
}

mod parser {
    use super::*;
    use nom::{
        branch::alt,
        bytes::complete::{tag, take_while1},
        combinator::{map, map_res, opt},
        sequence::{pair, preceded},
        IResult,
    };

    pub(crate) fn op(str: &str) -> IResult<&str, Op> {
        alt((
            map(tag("noop"), |_| Op::Noop),
            map(preceded(tag("addx "), number), Op::Addx),
        ))(str)
    }

    fn number(str: &str) -> IResult<&str, i64> {
        map_res(
            pair(opt(tag("-")), take_while1(|c: char| c.is_ascii_digit())),
            |(sign, s): (Option<&str>, &str)| {
                if sign.is_some() {
                    Ok(-s.parse::<i64>()?)
                } else {
                    s.parse()
                }
            },
        )(str)
    }
}
