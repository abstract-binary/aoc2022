use crate::OrError;
use clap::Parser;
use color_eyre::eyre::Context;
use eyre::eyre;
use itertools::Itertools;
use log::{debug, log_enabled, Level::Debug};
use nom::IResult;
use std::{
    collections::{HashMap, VecDeque},
    fs::File,
    io::{BufRead, BufReader},
    ops, str,
};

#[derive(Parser)]
pub struct Opts {
    file: String,

    #[clap(long)]
    rounds: usize,

    #[clap(long)]
    reduce_worry: bool,
}

pub fn run(opts: Opts) -> OrError<()> {
    let file = BufReader::new(File::open(&opts.file).wrap_err(format!("Reading {}", opts.file))?);
    itertools::process_results(file.lines(), |lines| {
        let mut monkeys = vec![];
        let mut expected_monkey_name = 0;
        for (_, group) in &lines.group_by(|l| l.is_empty()) {
            let monkey_str = group.collect::<Vec<String>>().join("\n");
            if !monkey_str.is_empty() {
                let monkey = monkey_str.parse::<Monkey>()?;
                assert!(monkey.name == expected_monkey_name);
                assert!(is_prime(monkey.test.divisible_by));
                monkeys.push(monkey);
                expected_monkey_name += 1;
            }
        }
        let mods: Vec<u64> = monkeys.iter().map(|m| m.test.divisible_by).collect();
        for monkey in &mut monkeys {
            for item in &mut monkey.items {
                item.set_mods(&mods);
            }
        }
        let mut items_inspected: Vec<usize> = vec![0; monkeys.len()];
        for _ in 0..opts.rounds {
            for idx in 0..monkeys.len() {
                items_inspected[idx] += monkeys[idx].items.len();
                while let Some(item) = monkeys[idx].items.pop_front() {
                    let item = monkeys[idx].operation.apply(&item);
                    let item = if opts.reduce_worry { item / 3 } else { item };
                    let next_monkey = monkeys[idx].test.next_monkey(&item);
                    monkeys[next_monkey].items.push_back(item);
                }
            }
        }
        if log_enabled!(Debug) {
            for (idx, items_seen) in items_inspected.iter().enumerate() {
                debug!("Monkey {idx} inspected items {items_seen} times.");
            }
        }
        let (items_seen1, items_seen2) = items_inspected
            .into_iter()
            .sorted_by(|a, b| b.cmp(a))
            .take(2)
            .collect_tuple()
            .unwrap();
        println!("Part 1: {}", items_seen1 * items_seen2);
        Ok::<(), eyre::Error>(())
    })??;
    Ok(())
}

fn is_prime(n: u64) -> bool {
    for x in 2..(n as f64).sqrt().ceil() as u64 {
        if n % x == 0 {
            return false;
        }
    }
    true
}

#[derive(Debug)]
struct ModNum {
    // The number, if it still fits into u64
    n: Option<u64>,
    // Result of the number modulo each key.
    mods: HashMap<u64, u64>,
}

impl ModNum {
    fn new(n: u64) -> Self {
        Self {
            n: Some(n),
            mods: HashMap::new(),
        }
    }

    fn set_mods(&mut self, mods: &[u64]) {
        let n = self.n.expect("cannot set mods if n is too large");
        self.mods = mods.iter().map(|x| (*x, n % x)).collect()
    }

    fn square(&self) -> Self {
        Self {
            n: self.n.and_then(|n| n.checked_mul(n)),
            mods: self.mods.iter().map(|(x, r)| (*x, (r * r) % x)).collect(),
        }
    }

    fn divisible_by(&self, divisible_by: u64) -> bool {
        0 == *self
            .mods
            .get(&divisible_by)
            .expect("unexpected mod in divisibility check")
    }
}

impl ops::Div<u64> for ModNum {
    type Output = ModNum;

    fn div(self, rhs: u64) -> Self::Output {
        let n = self.n.expect("cannot divide if n is too large");
        let mut t = Self::new(n / rhs);
        t.set_mods(self.mods.keys().copied().collect::<Vec<_>>().as_slice());
        t
    }
}

impl ops::Mul<u64> for &ModNum {
    type Output = ModNum;

    fn mul(self, rhs: u64) -> Self::Output {
        ModNum {
            n: self.n.and_then(|n| n.checked_mul(rhs)),
            mods: self.mods.iter().map(|(x, r)| (*x, (r * rhs) % x)).collect(),
        }
    }
}

impl ops::Add<u64> for &ModNum {
    type Output = ModNum;

    fn add(self, rhs: u64) -> Self::Output {
        ModNum {
            n: self.n.and_then(|n| n.checked_add(rhs)),
            mods: self.mods.iter().map(|(x, r)| (*x, (r + rhs) % x)).collect(),
        }
    }
}

#[derive(Debug)]
struct Monkey {
    name: usize,
    items: VecDeque<ModNum>,
    operation: Op,
    test: Test,
}

impl str::FromStr for Monkey {
    type Err = eyre::Error;

    fn from_str(str: &str) -> Result<Self, Self::Err> {
        match parse_monkey(str) {
            Err(err) => Err(eyre!("Error parsing monkey: {err}")),
            Ok(("", monkey)) => Ok(monkey),
            Ok((str, _)) => Err(eyre!("Left-over text parsing monkey: '{str}'")),
        }
    }
}

#[derive(Clone, Copy, Debug)]
enum Op {
    Mul(u64),
    Add(u64),
    Square,
}

impl Op {
    fn apply(&self, item: &ModNum) -> ModNum {
        match self {
            Op::Mul(x) => item * *x,
            Op::Add(x) => item + *x,
            Op::Square => item.square(),
        }
    }
}

#[derive(Debug)]
struct Test {
    divisible_by: u64,
    true_monkey: usize,
    false_monkey: usize,
}

impl Test {
    fn next_monkey(&self, item: &ModNum) -> usize {
        if item.divisible_by(self.divisible_by) {
            self.true_monkey
        } else {
            self.false_monkey
        }
    }
}

fn parse_monkey(str: &str) -> IResult<&str, Monkey> {
    use nom::{
        branch::alt,
        bytes::complete::{tag, take_while1},
        combinator::{map, map_res, opt},
        multi::separated_list1,
        sequence::{preceded, terminated},
    };
    fn number<N: str::FromStr>(str: &str) -> IResult<&str, N> {
        map_res(take_while1(|ch: char| ch.is_ascii_digit()), |s: &str| {
            s.parse()
        })(str)
    }
    fn ws(str: &str) -> IResult<&str, ()> {
        let (str, _) = take_while1(|ch: char| ch.is_whitespace())(str)?;
        Ok((str, ()))
    }
    fn op(str: &str) -> IResult<&str, Op> {
        alt((
            map(preceded(tag("* "), number), Op::Mul),
            map(preceded(tag("+ "), number), Op::Add),
            map(tag("* old"), |_| Op::Square),
        ))(str)
    }
    let (str, name) = terminated(preceded(tag("Monkey "), number), tag(":"))(str)?;
    let (str, ()) = ws(str)?;
    let (str, items) = preceded(tag("Starting items: "), separated_list1(tag(", "), number))(str)?;
    let (str, ()) = ws(str)?;
    let (str, operation) = preceded(tag("Operation: new = old "), op)(str)?;
    let (str, ()) = ws(str)?;
    let (str, divisible_by) = preceded(tag("Test: divisible by "), number)(str)?;
    let (str, ()) = ws(str)?;
    let (str, true_monkey) = preceded(tag("If true: throw to monkey "), number)(str)?;
    let (str, ()) = ws(str)?;
    let (str, false_monkey) = preceded(tag("If false: throw to monkey "), number)(str)?;
    let (str, _) = opt(ws)(str)?;
    Ok((
        str,
        Monkey {
            name,
            items: items.into_iter().map(ModNum::new).collect(),
            operation,
            test: Test {
                divisible_by,
                true_monkey,
                false_monkey,
            },
        },
    ))
}
