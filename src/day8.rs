use crate::OrError;
use clap::Parser;
use color_eyre::eyre::Context;
use eyre::eyre;
use log::{debug, log_enabled, Level::Debug};
use std::{
    fs::File,
    io::{BufRead, BufReader},
};

#[derive(Parser)]
pub struct Opts {
    file: String,
}

pub fn run(opts: Opts) -> OrError<()> {
    let file = BufReader::new(File::open(&opts.file).wrap_err(format!("Reading {}", opts.file))?);
    let mut grid: [[u8; 99]; 99] = [[0; 99]; 99];
    let mut width = 0;
    let mut height = 0;
    #[allow(clippy::explicit_counter_loop)]
    for line in file.lines() {
        let line = line?;
        width = 0;
        for ch in line.chars() {
            grid[height][width] =
                ch.to_digit(10)
                    .ok_or_else(|| eyre!("Not a digit: '{ch}'"))? as u8;
            width += 1;
        }
        height += 1;
    }
    macro_rules! debug_grid {
        ($grid:ident) => {
            if log_enabled!(Debug) {
                debug!("{}[{height}][{width}]", stringify!($grid));
                for line in $grid.iter().take(height) {
                    debug!("{:?}", &line[..width]);
                }
            }
        };
    }
    debug_grid!(grid);
    let mut max_height_e = [[0; 99]; 99];
    for i in 0..height {
        let mut max = 0;
        for j in 0..width {
            max = std::cmp::max(grid[i][j], max);
            max_height_e[i][j] = max;
        }
    }
    debug_grid!(max_height_e);
    let mut max_height_w = [[0; 99]; 99];
    for i in 0..height {
        let mut max = 0;
        for j in 0..width {
            max = std::cmp::max(grid[i][width - j - 1], max);
            max_height_w[i][width - j - 1] = max;
        }
    }
    debug_grid!(max_height_w);
    let mut max_height_s = [[0; 99]; 99];
    for j in 0..width {
        let mut max = 0;
        for i in 0..height {
            max = std::cmp::max(grid[i][j], max);
            max_height_s[i][j] = max;
        }
    }
    debug_grid!(max_height_s);
    let mut max_height_n = [[0; 99]; 99];
    for j in 0..width {
        let mut max = 0;
        for i in 0..height {
            max = std::cmp::max(grid[height - i - 1][j], max);
            max_height_n[height - i - 1][j] = max;
        }
    }
    debug_grid!(max_height_n);
    let mut visible = width * 2 + height * 2 - 4;
    for i in 1..height - 1 {
        for j in 1..width - 1 {
            if grid[i][j] > max_height_e[i][j - 1]
                || grid[i][j] > max_height_w[i][j + 1]
                || grid[i][j] > max_height_n[i + 1][j]
                || grid[i][j] > max_height_s[i - 1][j]
            {
                debug!("Visible grid[{i}][{j}]");
                visible += 1
            }
        }
    }
    println!("Part 1: {visible}");
    let mut max_score = 0;
    for i in 1..height - 1 {
        for j in 1..width - 1 {
            let mut score = 1;
            for k in j + 1..width {
                if grid[i][k] >= grid[i][j] || k == width - 1 {
                    score *= k - j;
                    debug!("score[e] = {}", k - j);
                    break;
                }
            }
            for k in 1..j + 1 {
                if grid[i][j - k] >= grid[i][j] || k == j {
                    score *= k;
                    debug!("score[w] = {}", k);
                    break;
                }
            }
            for k in i + 1..height {
                if grid[k][j] >= grid[i][j] || k == height - 1 {
                    score *= k - i;
                    debug!("score[s] = {}", k - i);
                    break;
                }
            }
            for k in 1..i + 1 {
                if grid[i - k][j] >= grid[i][j] || k == i {
                    score *= k;
                    debug!("score[n] = {}", k);
                    break;
                }
            }
            debug!("score[{i}][{j}] = {score}");
            max_score = std::cmp::max(score, max_score);
        }
    }
    println!("Part 2: {max_score}");
    Ok(())
}
