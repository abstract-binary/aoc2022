use crate::OrError;
use clap::Parser;
use color_eyre::eyre::Context;
use eyre::eyre;
use im::HashSet;
use itertools::Itertools;
use log::debug;
use std::{
    collections::HashMap,
    fs::File,
    io::{BufRead, BufReader},
};

#[derive(Parser)]
pub struct Opts {
    file: String,

    #[clap(long)]
    with_elephant: Option<u64>,
}

pub fn run(opts: Opts) -> OrError<()> {
    let file = BufReader::new(File::open(&opts.file).wrap_err(format!("Reading {}", opts.file))?);
    itertools::process_results(file.lines(), |lines| {
        let rooms: HashMap<String, Room> = lines
            .into_iter()
            .map(|l| {
                let r = parse_room(&l)?;
                Ok::<_, eyre::Error>((r.valve.clone(), r))
            })
            .try_collect()?;
        debug!("Rooms: {rooms:?}");
        let distances = compute_distances(&rooms);
        debug!("Distances: {distances:?}");
        let non_zero_flow = rooms
            .values()
            .filter_map(|room| {
                if room.flow > 0 {
                    Some(room.valve.clone())
                } else {
                    None
                }
            })
            .collect::<HashSet<_>>();
        debug!("Non-zero flow: {non_zero_flow:?}");
        let mut paths: HashMap<HashSet<String>, u64> = HashMap::new();
        compute_paths(
            "AA",
            30 - opts.with_elephant.unwrap_or(0),
            0,
            &rooms,
            &distances,
            &non_zero_flow,
            &non_zero_flow,
            &mut paths,
        );
        debug!("{paths:?}");
        if opts.with_elephant.is_some() {
            let mut max_flow = 0;
            for (p1, f1) in &paths {
                for (p2, f2) in &paths {
                    let i = p1.clone().intersection(p2.clone());
                    if i.is_empty() {
                        max_flow = std::cmp::max(max_flow, f1 + f2);
                    }
                }
            }
            println!("Part 2: {max_flow:?}");
        } else {
            let max_flow = paths.values().max().unwrap();
            println!("Part 1: {max_flow:?}");
        }
        Ok::<(), eyre::Error>(())
    })??;
    Ok(())
}

#[allow(clippy::too_many_arguments)]
fn compute_paths(
    cur: &str,
    minutes: u64,
    flow_so_far: u64,
    rooms: &HashMap<String, Room>,
    distances: &HashMap<(&String, &String), u64>,
    non_zero_flow: &HashSet<String>,
    unopened: &HashSet<String>,
    paths: &mut HashMap<HashSet<String>, u64>,
) {
    {
        let opened = non_zero_flow.clone().difference(unopened.clone());
        let entry = paths.entry(opened).or_default();
        *entry = std::cmp::max(*entry, flow_so_far);
    }
    for next in unopened {
        if let Some(dist) = distances.get(&(&cur.to_string(), next)) {
            if minutes > dist + 1 {
                let minutes = minutes - dist - 1;
                // debug!("From {cur} to {next}; {minutes} left");
                let mut unopened = unopened.clone();
                unopened.remove(next);
                let add_flow = rooms.get(next).unwrap().flow * minutes;
                compute_paths(
                    next,
                    minutes,
                    flow_so_far + add_flow,
                    rooms,
                    distances,
                    non_zero_flow,
                    &unopened,
                    paths,
                )
            }
        }
    }
}

// Floyd-Warshal
fn compute_distances(rooms: &HashMap<String, Room>) -> HashMap<(&String, &String), u64> {
    let mut dists = HashMap::new();
    for room in rooms.values() {
        for n in &room.neighbours {
            *dists.entry((&room.valve, n)).or_default() = 1;
        }
    }
    for middle in rooms.keys() {
        for start in rooms.keys() {
            for end in rooms.keys() {
                let new_dist = dists
                    .get(&(start, middle))
                    .and_then(|d_s_m| dists.get(&(middle, end)).map(|d_m_e| d_s_m + d_m_e));
                if let Some(new_dist) = new_dist {
                    let d_s_e: &mut u64 = dists.entry((start, end)).or_insert(new_dist);
                    if *d_s_e > new_dist {
                        *d_s_e = new_dist;
                    }
                }
            }
        }
    }
    dists
}

#[derive(Clone, Debug)]
struct Room {
    valve: String,
    flow: u64,
    neighbours: Vec<String>,
}

fn parse_room(str: &str) -> eyre::Result<Room> {
    use nom::{
        branch::alt,
        bytes::complete::tag,
        bytes::complete::take_while1,
        character::complete::u64,
        combinator::{all_consuming, map},
        multi::separated_list1,
        sequence::preceded,
        IResult,
    };
    fn valve(str: &str) -> IResult<&str, String> {
        map(
            take_while1(|ch: char| ch.is_ascii_uppercase()),
            |s: &str| s.to_string(),
        )(str)
    }
    fn room(str: &str) -> IResult<&str, Room> {
        let (str, v) = preceded(tag("Valve "), valve)(str)?;
        let (str, flow) = preceded(tag(" has flow rate="), u64)(str)?;
        let (str, neighbours) = alt((
            preceded(
                tag("; tunnels lead to valves "),
                separated_list1(tag(", "), valve),
            ),
            preceded(tag("; tunnel leads to valve "), map(valve, |v| vec![v])),
        ))(str)?;
        Ok((
            str,
            Room {
                valve: v,
                flow,
                neighbours,
            },
        ))
    }
    let (_, r) = all_consuming(room)(str).map_err(|err| eyre!("Parsing failure: {err}"))?;
    Ok(r)
}
