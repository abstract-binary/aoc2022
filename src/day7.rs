use crate::OrError;
use clap::Parser;
use color_eyre::eyre::Context;
use eyre::bail;
use itertools::Itertools;
use log::debug;
use std::{
    collections::HashMap,
    fs::File,
    io::{BufRead, BufReader},
};

#[derive(Parser)]
pub struct Opts {
    file: String,
}

pub fn run(opts: Opts) -> OrError<()> {
    let file = BufReader::new(File::open(&opts.file).wrap_err(format!("Reading {}", opts.file))?);
    let mut paths: HashMap<Vec<String>, u64> = HashMap::new();
    let mut cwd = vec![];
    for line in file.lines() {
        let line = line?;
        let line = line.trim();
        if line == "$ ls" {
            debug!("Ignornig ls command");
        } else if line == "$ cd /" {
            debug!("Handling `cd /`");
            cwd.clear()
        } else if line == "$ cd .." {
            debug!("Handling `cd ..`");
            cwd.pop();
        } else if let Some(dir) = line.strip_prefix("$ cd ") {
            debug!("Handling `cd {dir}`");
            cwd.push(dir.to_string());
        } else if line.starts_with("dir") {
            debug!("Ignornig dir-size line: {line}");
        } else {
            debug!("File-size line: {line}");
            match line.split_once(' ') {
                Some((size, name)) => {
                    let size = size.parse()?;
                    let mut path = cwd.clone();
                    path.push(name.to_string());
                    paths.insert(path, size);
                }
                None => bail!("Failed to parse file-size line: {line}"),
            }
        }
    }
    // Dirs with non-zero sizes.  We don't care about leaf dirs.
    let mut dir_sizes: HashMap<Vec<String>, u64> = HashMap::new();
    for (path, size) in &paths {
        let mut path = path.clone();
        while !path.is_empty() {
            path.pop();
            let entry = dir_sizes.entry(path.clone()).or_default();
            *entry += size;
        }
    }
    debug!("Files:");
    for (path, size) in paths.iter().sorted() {
        debug!("{}: {}", path.join("/"), size);
    }
    debug!("Dirs:");
    for (path, size) in dir_sizes.iter().sorted() {
        debug!("{}: {}", path.join("/"), size);
    }
    let part1: u64 = dir_sizes
        .iter()
        .filter_map(|(_, size)| if *size <= 100000 { Some(size) } else { None })
        .sum();
    println!("Part 1: {part1}");
    let fs_size = 70000000_u64;
    let update_size = 30000000_u64;
    let free_space = fs_size - dir_sizes.get(&vec![]).unwrap();
    if free_space < update_size {
        let mut dir_sizes: Vec<u64> = dir_sizes.into_iter().map(|(_, size)| size).collect();
        dir_sizes.sort();
        let to_delete = dir_sizes
            .into_iter()
            .find(|size| *size >= update_size - free_space)
            .unwrap();
        println!("Part 2: {to_delete}");
    }
    Ok(())
}
