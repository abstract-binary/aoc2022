use crate::OrError;
use clap::Parser;
use color_eyre::eyre::Context;
use eyre::eyre;
use itertools::Itertools;
use log::debug;
use std::{
    collections::HashSet,
    fs::File,
    io::{BufRead, BufReader},
    ops::RangeInclusive,
};

#[derive(Parser)]
pub struct Opts {
    file: String,

    #[clap(long)]
    line_y: Option<i64>,

    #[clap(long)]
    max_coord: Option<i64>,
}

pub fn run(opts: Opts) -> OrError<()> {
    let file = BufReader::new(File::open(&opts.file).wrap_err(format!("Reading {}", opts.file))?);
    itertools::process_results(file.lines(), |lines| {
        let mut beacons = HashSet::new();
        let mut sensors = HashSet::new();
        for l in lines {
            let (s, b) = parse_sensor_beacon(&l)?;
            beacons.insert(b);
            sensors.insert(s);
        }
        if let Some(line_y) = opts.line_y {
            let intersections =
                intersect_all_with_line(sensors.iter().map(|x| x as &dyn IntersectLine), line_y);
            let intersections = remove_intersections(&intersections, &beacons, line_y);
            debug!("Intersections: {intersections:?}");
            let mut positions = 0;
            for int in intersections {
                positions += int.end() - int.start() + 1;
            }
            println!("Positions where a beacon isn't: {positions}");
        }
        if let Some(max_coord) = opts.max_coord {
            for y in 0..=max_coord {
                if y % 10000 == 0 {
                    debug!("At {y}");
                }
                let intersections = intersect_all_with_line(
                    sensors
                        .iter()
                        .map(|x| x as &dyn IntersectLine)
                        .chain(beacons.iter().map(|x| x as &dyn IntersectLine)),
                    y,
                );
                let intersections = clamp_to_interval(&intersections, 0..=max_coord);
                let intersections = merge_adjoining(&intersections);
                if intersections.len() != 1 {
                    // 2636475: intersections: {3129626..=4000000, 0..=3129624}
                    // y = 2636475, x = 3129625
                    debug!("{y}: intersections: {intersections:?}")
                };
            }
        }
        Ok::<(), eyre::Error>(())
    })??;
    Ok(())
}

fn merge_adjoining(ranges: &HashSet<RangeInclusive<i64>>) -> HashSet<RangeInclusive<i64>> {
    let mut ranges = ranges.iter().sorted_by(|r1, r2| r1.start().cmp(r2.start()));
    let mut res = HashSet::new();
    if let Some(r1) = ranges.next() {
        let mut r1 = r1.clone();
        for r2 in ranges {
            if r1.end() + 1 == *r2.start() {
                r1 = *r1.start()..=*r2.end();
            } else {
                res.insert(r1);
                r1 = r2.clone();
            }
        }
        res.insert(r1);
    }
    res
}

fn clamp_to_interval(
    ranges: &HashSet<RangeInclusive<i64>>,
    limit: RangeInclusive<i64>,
) -> HashSet<RangeInclusive<i64>> {
    ranges
        .iter()
        .filter_map(|range| {
            if range.end() < limit.start() || range.start() > limit.end() {
                None
            } else {
                Some(
                    *std::cmp::max(range.start(), limit.start())
                        ..=*std::cmp::min(range.end(), limit.end()),
                )
            }
        })
        .collect()
}

fn remove_intersections(
    ranges: &HashSet<RangeInclusive<i64>>,
    beacons: &HashSet<Beacon>,
    line_y: i64,
) -> HashSet<RangeInclusive<i64>> {
    let mut res = HashSet::new();
    'outer: for range in ranges {
        for beacon in beacons {
            if beacon.y == line_y && range.contains(&beacon.x) {
                // debug!("Contained beacon: {beacon:?}");
                if beacon.x > *range.start() {
                    res.insert(*range.start()..=(beacon.x - 1));
                }
                if beacon.x < *range.end() {
                    res.insert((beacon.x + 1)..=*range.end());
                };
                continue 'outer;
            }
        }
        // debug!("Full range: {range:?}");
        res.insert(range.clone());
    }
    res
}

fn intersect_all_with_line<'a>(
    ints: impl Iterator<Item = &'a dyn IntersectLine>,
    line_y: i64,
) -> HashSet<RangeInclusive<i64>> {
    let mut intersections: HashSet<RangeInclusive<i64>> = HashSet::new();
    for int in ints {
        let mut int = int.intersect_line(line_y);
        // debug!("Intersection {s:?} with line: {int:?}");
        if int.end() - int.start() + 1 > 0 {
            while let Some(to_merge) = {
                intersections.iter().find_map(|i| {
                    if int.contains(i.start())
                        || int.contains(i.end())
                        || i.contains(int.start())
                        || i.contains(int.end())
                    {
                        Some(i.clone())
                    } else {
                        None
                    }
                })
            } {
                intersections.remove(&to_merge);
                let new_range = *std::cmp::min(to_merge.start(), int.start())
                    ..=*std::cmp::max(to_merge.end(), int.end());
                // debug!("  Merging with {to_merge:?}: {new_range:?}");
                int = new_range;
            }
            intersections.insert(int);
        }
    }
    intersections
}

trait IntersectLine {
    fn intersect_line(&self, line_y: i64) -> RangeInclusive<i64>;
}

#[derive(Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
struct Sensor {
    x: i64,
    y: i64,
    range: u64,
}

impl Sensor {
    fn new(sx: i64, sy: i64, b: &Beacon) -> Self {
        Self {
            x: sx,
            y: sy,
            range: dist(sx, sy, b.x, b.y),
        }
    }
}

impl IntersectLine for Sensor {
    fn intersect_line(&self, line_y: i64) -> RangeInclusive<i64> {
        let dist_y = (line_y - self.y).unsigned_abs();
        if dist_y > self.range {
            0..=0
        } else {
            let d = (self.range - dist_y) as i64;
            (self.x - d)..=(self.x + d)
        }
    }
}

fn dist(ax: i64, ay: i64, bx: i64, by: i64) -> u64 {
    (ax - bx).unsigned_abs() + (ay - by).unsigned_abs()
}

#[derive(Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
struct Beacon {
    x: i64,
    y: i64,
}

impl IntersectLine for Beacon {
    fn intersect_line(&self, line_y: i64) -> RangeInclusive<i64> {
        if line_y == self.y {
            self.x..=self.x
        } else {
            0..=0
        }
    }
}

fn parse_sensor_beacon(str: &str) -> eyre::Result<(Sensor, Beacon)> {
    use nom::{
        bytes::complete::tag,
        character::complete::i64,
        combinator::{all_consuming, map},
        sequence::{pair, preceded},
        IResult,
    };
    fn coords(str: &str) -> IResult<&str, (i64, i64)> {
        pair(preceded(tag("x="), i64), preceded(tag(", y="), i64))(str)
    }
    #[allow(clippy::type_complexity)]
    fn line(str: &str) -> IResult<&str, ((i64, i64), (i64, i64))> {
        pair(
            preceded(tag("Sensor at "), coords),
            preceded(tag(": closest beacon is at "), coords),
        )(str)
    }
    let (_, (s, b)) = {
        map(all_consuming(line), |((sx, sy), (bx, by))| {
            let b = Beacon { x: bx, y: by };
            (Sensor::new(sx, sy, &b), b)
        })(str)
        .map_err(|err| eyre!("Parsing failure: {err}"))?
    };
    Ok((s, b))
}
