use clap::{Parser, Subcommand};
use log::LevelFilter;
use simplelog::{ColorChoice, Config, TermLogger, TerminalMode};

type OrError<T> = color_eyre::Result<T>;

mod day1;
mod day10;
mod day11;
mod day12;
mod day13;
mod day14;
mod day15;
mod day16;
mod day17;
mod day2;
mod day3;
mod day4;
mod day5;
mod day6;
mod day7;
mod day8;
mod day9;

#[derive(Parser)]
struct Opts {
    #[clap(long)]
    debug: bool,

    #[clap(subcommand)]
    cmd: Cmd,
}

#[derive(Subcommand)]
enum Cmd {
    Day1(day1::Opts),
    Day2(day2::Opts),
    Day3(day3::Opts),
    Day4(day4::Opts),
    Day5(day5::Opts),
    Day6(day6::Opts),
    Day7(day7::Opts),
    Day8(day8::Opts),
    Day9(day9::Opts),
    Day10(day10::Opts),
    Day11(day11::Opts),
    Day12(day12::Opts),
    Day13(day13::Opts),
    Day14(day14::Opts),
    Day15(day15::Opts),
    Day16(day16::Opts),
    Day17(day17::Opts),
}

fn main() -> OrError<()> {
    color_eyre::install()?;
    let opts = Opts::parse();
    TermLogger::init(
        if opts.debug {
            LevelFilter::Debug
        } else {
            LevelFilter::Info
        },
        Config::default(),
        TerminalMode::Mixed,
        ColorChoice::Auto,
    )?;

    match opts.cmd {
        Cmd::Day1(x) => day1::run(x)?,
        Cmd::Day2(x) => day2::run(x)?,
        Cmd::Day3(x) => day3::run(x)?,
        Cmd::Day4(x) => day4::run(x)?,
        Cmd::Day5(x) => day5::run(x)?,
        Cmd::Day6(x) => day6::run(x)?,
        Cmd::Day7(x) => day7::run(x)?,
        Cmd::Day8(x) => day8::run(x)?,
        Cmd::Day9(x) => day9::run(x)?,
        Cmd::Day10(x) => day10::run(x)?,
        Cmd::Day11(x) => day11::run(x)?,
        Cmd::Day12(x) => day12::run(x)?,
        Cmd::Day13(x) => day13::run(x)?,
        Cmd::Day14(x) => day14::run(x)?,
        Cmd::Day15(x) => day15::run(x)?,
        Cmd::Day16(x) => day16::run(x)?,
        Cmd::Day17(x) => day17::run(x)?,
    }

    Ok(())
}
