use crate::OrError;
use clap::Parser;
use eyre::bail;
use itertools::Itertools;
use log::{debug, info, log_enabled, Level::Debug};
use std::{fmt, fs, time::Instant};

#[derive(Parser)]
pub struct Opts {
    file: String,

    #[clap(long)]
    num_rocks: usize,

    #[clap(long, default_value = "1000")]
    gc_interval: usize,

    #[clap(long, default_value = "10000")]
    log_interval: usize,
}

pub fn run(opts: Opts) -> OrError<()> {
    let jet_patterns: Vec<_> = fs::read_to_string(opts.file)?
        .trim()
        .chars()
        .map(|ch| {
            if ch == '<' {
                Ok(JetDir::Left)
            } else if ch == '>' {
                Ok(JetDir::Right)
            } else {
                bail!("Unknown jet dir: '{ch}'")
            }
        })
        .try_collect()?;
    debug!("Num rocks: {}", opts.num_rocks);
    debug!("Jet patterns: {jet_patterns:?}");
    let mut jets = jet_patterns.iter().cycle();
    let mut grid = Grid::new();
    let started = Instant::now();
    let mut last_log = started;
    for (round, rock) in ROCKS.iter().cycle().take(opts.num_rocks).enumerate() {
        if round % opts.gc_interval == 0 {
            grid.gc();
        }
        if round % opts.log_interval == 0 {
            let done = round as f64 / opts.num_rocks as f64;
            info!(
                "{:.2} done ({:.2}ms since last log; {:.2}mins to go)",
                done,
                last_log.elapsed().as_millis(),
                (started.elapsed().as_millis() as f64 / done * (1.0 - done)) / 1000.0 / 60.0
            );
            last_log = Instant::now();
        }
        debug!("Rock falling: {rock:?}");
        let mut x = 2;
        let mut y = grid.max_height() + STARTING_Y_OFFSET as i64;
        loop {
            let dir_x = match jets.next().unwrap() {
                JetDir::Left => -1,
                JetDir::Right => 1,
            };
            debug!("Jet dir: {dir_x}");
            if rock.can_move(&grid, x + dir_x, y) {
                x += dir_x;
                debug!("Moved!");
            }
            if rock.can_move_down(&grid, x, y - 1) {
                y -= 1;
            } else {
                debug!("x = {x}, y = {y}");
                rock.set_mask(&mut grid, x, y);
                if log_enabled!(Debug) {
                    eprintln!("Grid ({}):", grid.max_height());
                    eprintln!("{grid}");
                }
                debug!("Min_max_h = {}", grid.min_max_y());
                break;
            }
        }
    }
    println!("Max height: {}", grid.max_height());
    Ok(())
}

type Row = u8;

#[derive(Debug)]
enum JetDir {
    Left,
    Right,
}

#[derive(Debug)]
struct Rock {
    max_x: i64,
    masks: [u8; 4],
    masks_check: [u8; 2],
}

const STARTING_Y_OFFSET: usize = 3;

const ROCKS: &[Rock] = &[
    Rock {
        max_x: 3,
        masks: [0b1111, 0, 0, 0],
        masks_check: [0b1111, 0],
    },
    Rock {
        max_x: 2,
        masks: [0b10, 0b111, 0b10, 0],
        masks_check: [0b10, 0b111],
    },
    Rock {
        max_x: 2,
        masks: [0b111, 0b100, 0b100, 0],
        masks_check: [0b111, 0],
    },
    Rock {
        max_x: 0,
        masks: [0b1, 0b1, 0b1, 0b1],
        masks_check: [0b1, 0],
    },
    Rock {
        max_x: 1,
        masks: [0b11, 0b11, 0, 0],
        masks_check: [0b11, 0],
    },
];

trait Movable {
    fn can_move(&self, grid: &Grid, dx: i64, dy: i64) -> bool;
    fn can_move_down(&self, grid: &Grid, dx: i64, dy: i64) -> bool;

    fn set_mask(&self, grid: &mut Grid, dx: i64, dy: i64);
}

impl Movable for &Rock {
    fn can_move(&self, grid: &Grid, dx: i64, dy: i64) -> bool {
        0 <= dy && 0 <= dx && self.max_x + dx < 7 && {
            let oy = (dy - grid.offset_y) as usize + grid.grid_start;
            (self.masks[0] == 0 || (grid.grid[oy % GRID_LEN] & (self.masks[0] << dx) == 0))
                && (self.masks[1] == 0
                    || (grid.grid[(oy + 1) % GRID_LEN] & (self.masks[1] << dx) == 0))
                && (self.masks[2] == 0
                    || (grid.grid[(oy + 2) % GRID_LEN] & (self.masks[2] << dx) == 0))
                && (self.masks[3] == 0
                    || (grid.grid[(oy + 3) % GRID_LEN] & (self.masks[3] << dx) == 0))
        }
    }

    fn can_move_down(&self, grid: &Grid, dx: i64, dy: i64) -> bool {
        0 <= dy && 0 <= dx && self.max_x + dx < 7 && {
            let oy = (dy - grid.offset_y) as usize + grid.grid_start;
            (self.masks_check[0] == 0
                || (grid.grid[oy % GRID_LEN] & (self.masks_check[0] << dx) == 0))
                && (self.masks_check[1] == 0
                    || (grid.grid[(oy + 1) % GRID_LEN] & (self.masks_check[1] << dx) == 0))
        }
    }

    fn set_mask(&self, grid: &mut Grid, dx: i64, dy: i64) {
        for (ry, mask) in self.masks.iter().enumerate() {
            // grid.expand_to(dy + self.max_y);
            grid.set_mask_unchecked(*mask << dx, dy + ry as i64);
        }
    }
}

// Let's hope this is big enough
const GRID_LEN: usize = 20000;

struct Grid {
    grid: [Row; GRID_LEN],
    grid_start: usize,
    max_y: i64,
    offset_y: i64,
}

impl Grid {
    fn new() -> Self {
        Self {
            grid: [0; GRID_LEN],
            grid_start: 0,
            max_y: -1,
            offset_y: 0,
        }
    }

    fn set_mask_unchecked(&mut self, mask: u8, y: i64) {
        assert!(((y - self.offset_y) as usize) < GRID_LEN);
        let real_y = (((y - self.offset_y) as usize) + self.grid_start) % GRID_LEN;
        self.grid[real_y] |= mask;
        if mask > 0 {
            self.max_y = std::cmp::max(self.max_y, y);
        }
    }

    fn _contains_mask_unchecked(&self, x: i64, mask: u8, y: i64) -> bool {
        assert!(((y - self.offset_y) as usize) < GRID_LEN);
        mask > 0 && {
            let real_y = (((y - self.offset_y) as usize) + self.grid_start) % GRID_LEN;
            // debug!("Contains mask at {x}: {mask}");
            self.grid[real_y] & (mask << x) > 0
        }
    }

    fn max_height(&self) -> i64 {
        1 + self.max_y
    }

    // The minimum of the maximum heights of every row.
    fn min_max_y(&self) -> i64 {
        let mut min = i64::MAX;
        for mask in &[0b1, 0b10, 0b100, 0b1000, 0b10000, 0b100000, 0b1000000] {
            let mut max = 0;
            for y in 0..=(self.max_y - self.offset_y) {
                let y_inv = self.max_y - y;
                let real_y = (((y_inv - self.offset_y) as usize) + self.grid_start) % GRID_LEN;
                if self.grid[real_y] & mask > 0 {
                    max = y_inv;
                    break;
                }
            }
            min = std::cmp::min(max, min);
        }
        min
    }

    fn gc(&mut self) {
        let min_max_y = self.min_max_y();
        debug!("GC: min_max_y = {min_max_y}");
        for y in self.offset_y..min_max_y {
            let real_y = (((y - self.offset_y) as usize) + self.grid_start) % GRID_LEN;
            self.grid[real_y] = 0;
        }
        self.grid_start = ((min_max_y - self.offset_y) as usize + self.grid_start) % GRID_LEN;
        debug!("GC: grid_start = {}", self.grid_start);
        self.offset_y = min_max_y;
    }
}

impl fmt::Display for Grid {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        writeln!(f, "offset_y = {}", self.offset_y)?;
        let range = self.max_y - self.offset_y;
        for y in self.offset_y..=self.max_y {
            write!(f, "{:<4}|", self.offset_y + self.max_y - y)?;
            let real_y = (((range - (y - self.offset_y)) as usize) + self.grid_start) % GRID_LEN;
            for x in 0..7 {
                if self.grid[real_y] & (1u8 << x) > 0 {
                    write!(f, "#")?;
                } else {
                    write!(f, " ")?;
                }
            }
            writeln!(f)?;
        }
        Ok(())
    }
}
