use crate::OrError;
use clap::Parser;
use eyre::{bail, eyre, Context};
use itertools::Itertools;
use log::{debug, warn};
use std::{
    collections::HashSet,
    fs::File,
    io::{BufRead, BufReader},
};

#[derive(Parser)]
pub struct Opts {
    file: String,
}

pub fn run(opts: Opts) -> OrError<()> {
    let file = BufReader::new(File::open(&opts.file).wrap_err(format!("Reading {}", opts.file))?);
    let mut part1_total: u64 = 0;
    let mut part2_total: u64 = 0;
    for group in &file.lines().chunks(3) {
        let mut badge: Option<HashSet<u8>> = None;
        for line in group {
            let line = line?;
            debug!("Line: {line}");
            let line = line.as_bytes();
            let midway = line.len() / 2;
            if line.len() % 2 != 0 {
                warn!("Line length not even");
            }
            let half1 = line[..midway].iter().collect::<HashSet<&u8>>();
            let half2 = line[midway..].iter().collect::<HashSet<&u8>>();
            let bad_item = half1
                .intersection(&half2)
                .next()
                .ok_or_else(|| eyre!("No duplicate item in bag"))?;
            part1_total += item_score(**bad_item)?;
            // This is basically `reduce`, but looks a bit better
            if let Some(badge_set) = badge {
                badge = Some(
                    badge_set
                        .intersection(&line.iter().cloned().collect::<HashSet<u8>>())
                        .cloned()
                        .collect(),
                )
            } else {
                badge = Some(line.iter().cloned().collect::<HashSet<u8>>());
            }
        }
        part2_total += item_score(
            badge
                .and_then(|x| x.iter().cloned().next())
                .ok_or_else(|| eyre!("Not enough elves in group"))?,
        )?;
    }
    println!("Part1: {part1_total}");
    println!("Part1: {part2_total}");
    Ok(())
}

fn item_score(item: u8) -> OrError<u64> {
    let ch = item as char;
    if ch.is_ascii_lowercase() {
        Ok(1 + (item - b'a') as u64)
    } else if ch.is_ascii_uppercase() {
        Ok(27 + (item - b'A') as u64)
    } else {
        bail!("Non-letter character: '{ch}'");
    }
}
