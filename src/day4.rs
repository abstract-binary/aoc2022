#[allow(unused_imports)]
use log::debug;

use crate::OrError;
use clap::Parser;
use eyre::{bail, Context};
use std::{
    fs::File,
    io::{BufRead, BufReader},
    str,
};

#[derive(Parser)]
pub struct Opts {
    file: String,
}

pub fn run(opts: Opts) -> OrError<()> {
    let file = BufReader::new(File::open(&opts.file).wrap_err(format!("Reading {}", opts.file))?);
    let mut part1_total = 0;
    let mut part2_total = 0;
    for line in file.lines() {
        let line = line?;
        match line.split_once(',') {
            Some((r1, r2)) => {
                let r1: Range = r1.parse()?;
                let r2: Range = r2.parse()?;
                if r1.contains(&r2) || r2.contains(&r1) {
                    part1_total += 1;
                }
                if r1.overlaps(&r2) {
                    part2_total += 1;
                }
            }
            None => bail!("Cannot find range pair on line '{line}'"),
        }
    }
    println!("Part 1: {part1_total}");
    println!("Part 2: {part2_total}");
    Ok(())
}

#[derive(Debug)]
struct Range {
    lo: u64,
    hi: u64,
}

impl Range {
    pub fn contains(&self, other: &Range) -> bool {
        self.lo <= other.lo && other.hi <= self.hi
    }

    pub fn overlaps(&self, other: &Range) -> bool {
        std::cmp::max(self.lo, other.lo) <= std::cmp::min(self.hi, other.hi)
    }
}

impl str::FromStr for Range {
    type Err = eyre::Error;

    fn from_str(str: &str) -> Result<Self, Self::Err> {
        match str.split_once('-') {
            Some((lo, hi)) => Ok(Self {
                lo: lo.parse()?,
                hi: hi.parse()?,
            }),
            None => bail!("Cannot parse range '{str}'"),
        }
    }
}
