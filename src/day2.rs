use crate::OrError;
use clap::Parser;
use color_eyre::eyre::Context;
use std::{
    fs::File,
    io::{BufRead, BufReader},
};

#[derive(Parser)]
pub struct Opts {
    file: String,
}

pub fn run(opts: Opts) -> OrError<()> {
    let file = BufReader::new(File::open(&opts.file).wrap_err(format!("Reading {}", opts.file))?);
    macro_rules! set {
        ($arr:ident, $ch1:literal, $ch2:literal, $val:expr) => {
            $arr[($ch1 - b'A') as usize][($ch2 - b'X') as usize] = $val;
        };
    }
    macro_rules! get {
        ($arr:ident, $idx1:expr, $idx2:expr) => {
            $arr[(($idx1) - b'A') as usize][(($idx2) - b'X') as usize]
        };
    }
    let part1_arr = {
        let mut arr: [[usize; 3]; 3] = [[0, 0, 0], [0, 0, 0], [0, 0, 0]];
        // A - Rock, B - Paper, C - Scissors
        // X - Rock, Y - Paper, Z - Scissors
        // Score: Rock = 1, Paper = 2, Scissors = 3
        // Score: loss = 0, draw = 3, win = 6
        set!(arr, b'A', b'X', 4);
        set!(arr, b'A', b'Y', 8);
        set!(arr, b'A', b'Z', 3);
        set!(arr, b'B', b'X', 1);
        set!(arr, b'B', b'Y', 5);
        set!(arr, b'B', b'Z', 9);
        set!(arr, b'C', b'X', 7);
        set!(arr, b'C', b'Y', 2);
        set!(arr, b'C', b'Z', 6);
        arr
    };
    let part2_arr = {
        let mut arr: [[usize; 3]; 3] = [[0, 0, 0], [0, 0, 0], [0, 0, 0]];
        // A - Rock, B - Paper, C - Scissors
        // X - lose, Y - draw, Z - win
        // Score: Rock = 1, Paper = 2, Scissors = 3
        // Score: loss = 0, draw = 3, win = 6
        set!(arr, b'A', b'X', 3); // pick scissors
        set!(arr, b'A', b'Y', 4); // pick rock
        set!(arr, b'A', b'Z', 8); // pick paper
        set!(arr, b'B', b'X', 1); // pick rock
        set!(arr, b'B', b'Y', 5); // pick paper
        set!(arr, b'B', b'Z', 9); // pick scissors
        set!(arr, b'C', b'X', 2); // pick paper
        set!(arr, b'C', b'Y', 6); // pick scissors
        set!(arr, b'C', b'Z', 7); // pick rock
        arr
    };
    let mut part1_score = 0;
    let mut part2_score = 0;
    for line in file.lines() {
        let line = line?;
        let line = line.as_bytes();
        part1_score += get!(part1_arr, line[0], line[2]);
        part2_score += get!(part2_arr, line[0], line[2]);
    }
    println!("Part1: {part1_score}");
    println!("Part2: {part2_score}");
    Ok(())
}
