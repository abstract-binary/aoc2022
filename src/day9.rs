use crate::OrError;
use clap::Parser;
use color_eyre::eyre::Context;
use eyre::bail;
use log::debug;
use std::{
    collections::HashSet,
    fmt,
    fs::File,
    io::{BufRead, BufReader},
};

#[derive(Parser)]
pub struct Opts {
    file: String,
    #[clap(long)]
    rope_length: usize,
}

pub fn run(opts: Opts) -> OrError<()> {
    let file = BufReader::new(File::open(&opts.file).wrap_err(format!("Reading {}", opts.file))?);
    let mut rope = Rope::new(opts.rope_length)?;
    debug!("   {rope:?}");
    let mut tail_positions = HashSet::new();
    tail_positions.insert(rope.tail_pos());
    for line in file.lines() {
        let line = line?;
        let motion = parse_motion(&line)?;
        rope.move_head(motion, &mut tail_positions);
        debug!("-> {rope:?}, via {motion}");
    }
    println!("Tail positions: {}", tail_positions.len());
    Ok(())
}

struct Rope {
    knots: Vec<Pos>,
}

impl Rope {
    fn new(len: usize) -> eyre::Result<Self> {
        if len < 2 {
            bail!("Rope must be at least 2 long");
        }
        Ok(Self {
            knots: vec![Pos::default(); len],
        })
    }

    fn move_head(&mut self, mut motion: Motion, tail_positions: &mut HashSet<Pos>) {
        while let Some((mx, my)) = motion.movement() {
            self.knots[0].x += mx;
            self.knots[0].y += my;
            for i in 1..self.knots.len() {
                if let Some((dx, dy)) = self.knots[i - 1].elastic_pull(&self.knots[i]) {
                    self.knots[i].x += dx;
                    self.knots[i].y += dy;
                }
            }
            tail_positions.insert(self.tail_pos());
        }
    }

    fn tail_pos(&self) -> Pos {
        self.knots[self.knots.len() - 1]
    }
}

impl fmt::Debug for Rope {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "H: {}, T: {}", self.knots[0], self.tail_pos())
    }
}

#[derive(Default, Clone, Copy, Eq, Hash, Ord, PartialEq, PartialOrd)]
struct Pos {
    x: i64,
    y: i64,
}

impl Pos {
    // Pull `other` towards `self` if necessary by the returned
    // amount.  This only when the distance is at most 2.
    fn elastic_pull(&self, other: &Self) -> Option<(i64, i64)> {
        #[allow(clippy::collapsible_else_if)]
        if self.x == other.x {
            if (self.y - other.y).abs() <= 1 {
                None
            } else {
                Some((0, (self.y - other.y).signum()))
            }
        } else if self.y == other.y {
            if (self.x - other.x).abs() <= 1 {
                None
            } else {
                Some(((self.x - other.x).signum(), 0))
            }
        } else {
            if (self.x - other.x).abs() == 1 && (self.y - other.y).abs() == 1 {
                None
            } else {
                Some(((self.x - other.x).signum(), (self.y - other.y).signum()))
            }
        }
    }
}

impl fmt::Display for Pos {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}

#[derive(Clone, Copy)]
pub struct Motion {
    dir: Dir,
    dist: u64,
}

impl fmt::Display for Motion {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:?}({})", self.dir, self.dist)
    }
}

#[derive(Clone, Copy, Debug)]
pub enum Dir {
    Up,
    Down,
    Left,
    Right,
}

impl Motion {
    fn movement(&mut self) -> Option<(i64, i64)> {
        if self.dist == 0 {
            None
        } else {
            self.dist -= 1;
            match self.dir {
                Dir::Up => Some((0, -1)),
                Dir::Down => Some((0, 1)),
                Dir::Left => Some((-1, 0)),
                Dir::Right => Some((1, 0)),
            }
        }
    }
}

fn parse_motion(str: &str) -> eyre::Result<Motion> {
    match parser::motion(str) {
        Err(err) => bail!("Error parsing '{str}': {err}"),
        Ok(("", motion)) => Ok(motion),
        Ok((left_over, _)) => bail!("Left-over parsing '{str}': '{left_over}'"),
    }
}

mod parser {
    use super::*;
    use nom::{
        branch::alt,
        bytes::complete::{tag, take_while1},
        combinator::{map, map_res},
        sequence::preceded,
        IResult,
    };

    pub(crate) fn motion(str: &str) -> IResult<&str, Motion> {
        alt((
            map(preceded(tag("U "), number), |n| Motion {
                dir: Dir::Up,
                dist: n,
            }),
            map(preceded(tag("D "), number), |n| Motion {
                dir: Dir::Down,
                dist: n,
            }),
            map(preceded(tag("L "), number), |n| Motion {
                dir: Dir::Left,
                dist: n,
            }),
            map(preceded(tag("R "), number), |n| Motion {
                dir: Dir::Right,
                dist: n,
            }),
        ))(str)
    }

    fn number(str: &str) -> IResult<&str, u64> {
        map_res(take_while1(|c: char| c.is_ascii_digit()), |s: &str| {
            s.parse()
        })(str)
    }
}
