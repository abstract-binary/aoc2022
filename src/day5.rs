#[allow(unused_imports)]
use log::debug;

use crate::OrError;
use clap::Parser;
use eyre::{bail, eyre, Context};
use itertools::Itertools;
use std::{
    fs::File,
    io::{BufRead, BufReader},
    str,
};

#[derive(Parser)]
pub struct Opts {
    file: String,
}

pub fn run(opts: Opts) -> OrError<()> {
    let file = BufReader::new(File::open(&opts.file).wrap_err(format!("Reading {}", opts.file))?);
    let mut stack_lines = vec![];
    let mut stacks = None;
    for line in file.lines() {
        let line = line?;
        match stacks.as_mut() {
            None => {
                if line.is_empty() {
                    let sts = Stacks::new(&stack_lines)?;
                    stacks = Some((sts.clone(), sts));
                    debug!("{stacks:?}");
                } else {
                    stack_lines.push(line);
                }
            }
            Some((stacks1, stacks2)) => {
                let op = Op::parse(&line)?;
                debug!("Op: {op:?}");
                for _ in 0..op.count {
                    let crt = stacks1.stacks[op.src - 1].pop().unwrap();
                    stacks1.stacks[op.dest - 1].push(crt);
                }
                let src_stack = &mut stacks2.stacks[op.src - 1];
                let to_move: Vec<Crate> = src_stack[(src_stack.len() - op.count)..].to_vec();
                src_stack.truncate(src_stack.len() - op.count);
                stacks2.stacks[op.dest - 1].extend_from_slice(&to_move);
            }
        }
    }
    debug!("Stacks: {stacks:?}");
    let (stacks1, stacks2) = stacks.unwrap_or_default();
    println!("Part 1: {}", stacks1.top_str());
    println!("Part 2: {}", stacks2.top_str());
    Ok(())
}

#[derive(Clone, Copy, Debug)]
struct Crate(char);

impl str::FromStr for Crate {
    type Err = eyre::Error;

    fn from_str(str: &str) -> Result<Self, Self::Err> {
        let chars: Vec<_> = str.chars().collect();
        if chars.len() != 3 || chars[0] != '[' || chars[2] != ']' {
            bail!("Invalid crate: '{str}'");
        }
        Ok(Self(chars[1]))
    }
}

#[derive(Clone, Debug, Default)]
struct Stacks {
    stacks: Vec<Vec<Crate>>,
}

impl Stacks {
    fn new(lines: &[String]) -> eyre::Result<Self> {
        if lines.is_empty() {
            bail!("No lines given to Stacks parser")
        }
        let mut rev_lines = lines.iter().rev();
        let max_num = {
            let line = rev_lines.next().unwrap();
            let mut res = vec![];
            let mut str = line.as_str();
            while !str.is_empty() {
                let (new_str, n) =
                    parse_ws_num(str).map_err(|err| eyre!("Parsing number line: {err}"))?;
                str = new_str;
                res.push(n);
            }
            *res.iter().max().unwrap()
        };
        let mut stacks = Vec::with_capacity(max_num);
        stacks.resize(max_num, vec![]);
        for line in rev_lines {
            let chunks = line.chars().chunks(4);
            let crates = chunks
                .into_iter()
                .map(|str| {
                    let str: String = str.into_iter().join("");
                    let str = str.trim();
                    if str.is_empty() {
                        Ok(None)
                    } else {
                        str.parse().map(Option::Some)
                    }
                })
                .collect::<Result<Vec<Option<Crate>>, _>>()?;
            debug!("crates on line: {crates:?}");
            for (idx, crt) in crates.iter().enumerate() {
                if let Some(crt) = crt {
                    stacks[idx].push(*crt);
                }
            }
        }
        Ok(Stacks { stacks })
    }

    fn top_str(&self) -> String {
        let mut res = String::new();
        for stack in &self.stacks {
            match stack.last() {
                None => res.push(' '),
                Some(Crate(ch)) => res.push(*ch),
            }
        }
        res
    }
}

#[derive(Debug)]
struct Op {
    count: usize,
    src: usize,
    dest: usize,
}

impl Op {
    fn parse(str: &str) -> eyre::Result<Self> {
        match Self::parse_int(str).map_err(|err| eyre!("Parsing Op: '{err}'"))? {
            ("", t) => Ok(t),
            (left_over, _) => bail!("left-over parsing Op: '{left_over}'"),
        }
    }

    fn parse_int(str: &str) -> nom::IResult<&str, Self> {
        use nom::bytes::complete::tag;
        let (str, _) = tag("move")(str)?;
        let (str, count) = parse_ws_num(str)?;
        let (str, _) = parse_ws(str)?;
        let (str, _) = tag("from")(str)?;
        let (str, src) = parse_ws_num(str)?;
        let (str, _) = parse_ws(str)?;
        let (str, _) = tag("to")(str)?;
        let (str, dest) = parse_ws_num(str)?;
        Ok((str, Self { count, src, dest }))
    }
}

fn parse_ws_num<T: str::FromStr>(str: &str) -> nom::IResult<&str, T> {
    use nom::{bytes::complete::take_while1, combinator::map_res};
    let (str, _) = parse_ws(str)?;
    let (str, num) = map_res(take_while1(|c: char| c.is_ascii_digit()), |str: &str| {
        str.parse()
    })(str)?;
    Ok((str, num))
}

fn parse_ws(str: &str) -> nom::IResult<&str, ()> {
    use nom::bytes::complete::take_while1;
    let (str, _) = take_while1(char::is_whitespace)(str)?;
    Ok((str, ()))
}
