use crate::OrError;
use clap::Parser;
use std::{
    collections::BinaryHeap,
    fs::File,
    io::{BufRead, BufReader},
    str::FromStr,
};

#[derive(Parser)]
pub struct Opts {
    file: String,
}

pub fn run(opts: Opts) -> OrError<()> {
    let file = BufReader::new(File::open(opts.file)?);
    let mut elf_kcal: BinaryHeap<u64> = BinaryHeap::new();
    let mut cur_elf_kcal: u64 = 0;
    for line in file.lines() {
        let line = line?;
        match line.as_str() {
            "" => {
                elf_kcal.push(cur_elf_kcal);
                cur_elf_kcal = 0;
            }
            str => cur_elf_kcal += u64::from_str(str)?,
        }
    }
    println!("Max1: {}", elf_kcal.peek().unwrap());
    println!(
        "Max3: {}",
        elf_kcal.pop().unwrap() + elf_kcal.pop().unwrap() + elf_kcal.pop().unwrap()
    );
    Ok(())
}
