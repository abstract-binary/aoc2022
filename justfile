default:
	just --choose

# check format and compilation
check:
	cargo fmt --all
	cargo check --workspace

# run all tests
test:
	cargo nextest run --workspace

# debug build
debug: check
	cargo build

# release build
release: check
	cargo build --release --workspace

# clean files unknown to git
clean:
	git clean -dxf

# run clippy
clippy:
	cargo clippy --workspace

pgo-day17:
	rm -rf /tmp/pgo-data
	RUSTFLAGS="-Cprofile-generate=/tmp/pgo-data" cargo build --release
	target/release/aoc2022 day17 --num-rocks 100000000 inputs/day17/part1.txt --gc-interval 2000 --log-interval 10000000
	llvm-profdata merge -o /tmp/pgo-data/merged.profdata /tmp/pgo-data
	RUSTFLAGS="-Cprofile-use=/tmp/pgo-data/merged.profdata" cargo build --release
